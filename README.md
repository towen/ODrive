TOwen's fork of Odrive
======================

Intended work:
* Python:
  * CAN-bus control library
  * remote device data model
  * PyQT GUI
* Firmware:
  * Emergency Stop Button support
  * Improved CAN Bus protocol
    * Message based priority instead of unit ID priority
    * Repeated status messages
    * CAN-FD in future (supporting the same fibre protocol as USB)
  * Improved USB protocol
    * 64 byte status message with:
       * Both axes position, velocity, torque 
       * controller following error
* PCB:
  * KiCAD implementation of single-channel motor-mounted controller
    * 3ch current sensing
    * Newer gate driver chip
    * faster CPU

![ODrive Logo](https://static1.squarespace.com/static/58aff26de4fcb53b5efd2f02/t/59bf2a7959cc6872bd68be7e/1505700483663/Odrive+logo+plus+text+black.png?format=1000w)

This project is all about accurately driving brushless motors, for cheap. The aim is to make it possible to use inexpensive brushless motors in high performance robotics projects, like [this](https://www.youtube.com/watch?v=WT4E5nb3KtY).

| Branch | Build Status |
|--------|--------------|
| master | [![Build Status](https://travis-ci.org/madcowswe/ODrive.png?branch=master)](https://travis-ci.org/madcowswe/ODrive) |
| devel  | [![Build Status](https://travis-ci.org/madcowswe/ODrive.png?branch=devel)](https://travis-ci.org/madcowswe/ODrive) |


Please refer to the [Developer Guide](https://docs.odriverobotics.com/developer-guide) to get started with ODrive firmware development.


### Repository Structure
 * **Firmware**: ODrive firmware
 * **tools**: Python library & tools
 * **docs**: Documentation

### Other Resources

 * [Main Website](https://www.odriverobotics.com/)
 * [User Guide](https://docs.odriverobotics.com/)
 * [Forum](https://discourse.odriverobotics.com/)
 * [Chat](https://discourse.odriverobotics.com/t/come-chat-with-us/281)
