#!/usr/bin/env python3
import sys
try:
  from PyQt5 import QtCore, QtGui, QtWidgets
  from PyQt5.Qt import Qt
  from PyQt5.QtWidgets import QMainWindow, QWidget, QApplication, QComboBox, QStyleFactory, QLabel, QCheckBox, QGroupBox, QVBoxLayout, QPushButton, QGridLayout, QTableView, QTableWidget, QListView, QFrame, QHBoxLayout, QComboBox, QLineEdit, QHeaderView, QSizePolicy, QLCDNumber
  from PyQt5.QtCore import QAbstractItemModel, QAbstractTableModel, QAbstractListModel, QVariant, QModelIndex, QObject
  from PyQt5.QtGui import QPalette, QColor
  import argparse
  

  import numpy as np
  from CANsimple import ODrive_Proto_CANsimple, ODriveBusModel, ODriveAxisModel, ODriveCANBus, UpdaterThread

  from enums import ODenum, ODerror

  from PyQt5.Qwt import QwtPlot, QwtPlotCurve, QwtDial, QwtDialSimpleNeedle, QwtThermo
  
  from QLed import QLed

  import can
  

except ImportError as e:
  print("Failed to import a module: %s" % e)
  print("On Debian and Ubuntu systems:")
  print("sudo apt install python3-pyqt5 python3-sortedcontainers python3-qwt python3-can python3-pyqt5.qwt")
  sys.exit(0)


#ODriveBus = ODriveCANBus() # defined later in __main__
ODriveProto = ODrive_Proto_CANsimple()
ODriveModel = ODriveBusModel(test=False)
SelectedNode = 1

class ODriveGUIWindow(QMainWindow):
    def __init__(self, parent=None,  args=[]):
        super(ODriveGUIWindow, self).__init__(parent)
        mw = ODriveGUI(self,  args)
        self.setCentralWidget(mw)
        self.setWindowTitle("ODrive CAN Bus Control GUI")
    #    self.statusBar()
    #    mainMenu = self.menuBar()
        quitAction = QtWidgets.QAction("&Quit", self)
        quitAction.setShortcut("Ctrl+Q")
        quitAction.setStatusTip('Leave The App')
        quitAction.triggered.connect(self.close_application)
   #     fileMenu = mainMenu.addMenu('&File')
   #     fileMenu.addAction(quitAction)
        
    def close_application(self):
        sys.exit(0)
            
             
class ODriveGUI(QWidget):
    def __init__(self, parent=None,  args=[]):
        super(ODriveGUI, self).__init__(parent)
        
        #self.bus = DummyBus();

        self.originalPalette = QApplication.palette()
        
        self.GraphGroupBox = QGroupBox("Plotter")
        self.CommandsGroupBox = QGroupBox("General Commands")
        self.StatusGroupBox = QGroupBox("Axis Status")
        self.DevicesGroupBox = QGroupBox("Detected Devices")
        self.DataGroupBox = QGroupBox("Data Dictionary")
        self.DialsGroupBox = QGroupBox("Manual Control")
        
        self.makeControlsGroupBox()
        self.makeStatusGroupBox()
        self.makeCommandsGroupBox()
        self.makeDialsGroupBox()
        self.makeDeviceList()
        self.makeGraphArea()
        subLayout = QVBoxLayout()
        
        self.makeDataTable()
        mainLayout = QGridLayout()
        subLayout.addWidget(self.ControlsGroupBox)
        subLayout.addWidget(self.StatusGroupBox)
        subLayout.addWidget(self.CommandsGroupBox)
        subLayout.addWidget(self.DevicesGroupBox)
        mainLayout.addLayout(subLayout, 0, 0)
        
        subLayout = QVBoxLayout()
        subLayout.addWidget(self.DataGroupBox)
        subLayout.addWidget(self.DialsGroupBox)
        mainLayout.addLayout(subLayout, 0, 1)
        
        
        subLayout = QVBoxLayout()
        subLayout.addWidget(self.GraphGroupBox)
        
        mainLayout.addLayout(subLayout, 0, 2)
        
        mainLayout.setRowStretch(0, 1)
        mainLayout.setColumnStretch(0, 0)
        mainLayout.setColumnStretch(1, 1)
        mainLayout.setColumnStretch(2, 2)
        self.setLayout(mainLayout)
        
    def makeControlsGroupBox(self):
        self.ControlsGroupBox = QGroupBox("State Machine Controls")
        enablePushButton = QPushButton("Request Closed Loop Control (F5)")
        idlePushButton = QPushButton("Request Idle state (F4)")
        calibPushButton = QPushButton("Request Encoder Offset Calibration (F2)")
        estopPushButton = QPushButton("Emergency Stop (ESC)")
        resetErrorsPushButton = QPushButton("Reset Errors (F12)")
        rebootPushButton = QPushButton("Reboot Drive")
        
        estopPushButton.clicked.connect(lambda:self.sendCommand("STOP"))
        enablePushButton.clicked.connect(lambda:self.sendCommand("SetRequestedState", ODenum.axis_state_r["AXIS_STATE_CLOSED_LOOP_CONTROL"]))
        idlePushButton.clicked.connect(lambda:self.sendCommand("SetRequestedState", ODenum.axis_state_r["AXIS_STATE_IDLE"]))
        calibPushButton.clicked.connect(lambda:self.sendCommand("SetRequestedState", ODenum.axis_state_r["AXIS_STATE_FULL_CALIBRATION_SEQUENCE"]))
        estopPushButton.clicked.connect(lambda:self.sendCommand("ClearErrors"))
        rebootPushButton.clicked.connect(lambda:self.sendCommand("Reboot"))
        
        layout = QVBoxLayout()
        
        layout.addWidget(estopPushButton)
        layout.addWidget(enablePushButton)
        layout.addWidget(idlePushButton)
        layout.addWidget(calibPushButton)
        layout.addWidget(resetErrorsPushButton)
        layout.addWidget(rebootPushButton)

    
        layout.addStretch(1)
        self.ControlsGroupBox.setLayout(layout)
        
    def makeCommandsGroupBox(self):
        commands = ODriveProto.get_cmds()
        self.commandComboBox = QComboBox(self)
        commandLabel = QLabel("Command")
        self.commandComboBox.addItems(sorted(commands))
        commandLabel = QLabel("cmd name: ")
        #commandNameTextBox = QLineEdit(self)
        commandNumTextBox = QLineEdit(self)
        commandNumLabel = QLabel("cmd num: ")
        self.args1TextBox = QLineEdit(self)
        args1Label = QLabel("arg 1: ")
        self.args2TextBox = QLineEdit(self)
        args2Label = QLabel("arg 2: ")
        self.args3TextBox = QLineEdit(self)
        args3Label = QLabel("arg 3: ")
        commandPushButton = QPushButton("Send")
        layout = QVBoxLayout()
        subLayout = QGridLayout()
        subLayout.addWidget(commandLabel,1,1)
        subLayout.addWidget(self.commandComboBox,1,2)
        subLayout.addWidget(commandNumLabel,2,1)
        subLayout.addWidget(commandNumTextBox,2,2)
        #subLayout.addWidget(commandNameTextBox,2,1)
        subLayout.addWidget(self.args1TextBox,3,2)
        subLayout.addWidget(args1Label,3,1)
        subLayout.addWidget(self.args2TextBox,4,2)
        subLayout.addWidget(args2Label,4,1)
        subLayout.addWidget(self.args3TextBox,5,2)
        subLayout.addWidget(args3Label,5,1)
        layout.addLayout(subLayout)
        layout.addWidget(commandPushButton)
        commandPushButton.clicked.connect(self.commandButtonPushed)
        layout.addStretch(1)
        self.CommandsGroupBox.setLayout(layout)

    def makeStatusGroupBox(self):
       layout = QVBoxLayout()
       subLayout = QGridLayout()
       
       self.statusLed=QLed(self, onColour=QLed.Red, shape=QLed.Circle)
       subLayout.addWidget(self.statusLed,4,2)
       
       self.positionLCD = QLCDNumber(self)
       positionLabel = QLabel("Position", self)
       subLayout.addWidget(positionLabel,1,1)
       subLayout.addWidget(self.positionLCD,1,2)
       
       self.velocityLCD = QLCDNumber(self)
       velocityLabel = QLabel("Velocity", self)
       subLayout.addWidget(velocityLabel,2,1)
       subLayout.addWidget(self.velocityLCD,2,2)
       
       self.currentLCD = QLCDNumber(self)
       currentLabel = QLabel("Current", self)
       subLayout.addWidget(currentLabel,3,1)
       subLayout.addWidget(self.currentLCD,3,2)
       
       layout.addLayout(subLayout)
       self.StatusGroupBox.setLayout(layout)
    
    def makeDialsGroupBox(self):
        layout = QHBoxLayout()

        self.positionDial = doubleDial(self) 
        self.positionDial.setUpperBound(16384)
        self.positionDial.setWrapping(True)
        self.positionDial.setTracking(True)
        self.positionDial.setReadOnly(False)
        self.positionDial.setMinimumHeight(350)
        self.positionDial.setMinimumWidth(350)
        self.positionDial.setPageSteps(100)
        self.positionDial.setNeedle(QwtDialSimpleNeedle(QwtDialSimpleNeedle.Arrow, True, QColor(Qt.red)))
        #self.positionDial.setNeedle2(self.encoderNeedle)
        self.positionDial.setMode(QwtDial.RotateNeedle)
        self.positionDial.valueChanged.connect(self.setPositionDial)
        layout.addWidget(self.positionDial)
        
        self.positionError = QwtThermo(self)
        layout.addWidget(self.positionError)
         
        self.DialsGroupBox.setLayout(layout)
        
    def setPositionDial(self, value):
        ODriveBus.send_message(self.getSelectedNodeID(), "SetInputPos", value)
    
    def makeDeviceList(self):
        clearListPushButton = QPushButton("Clear Device List")
        clearListPushButton.clicked.connect(self.clearDeviceList)

        self.deviceList = QTableView()
        self.deviceListModel = ODriveDeviceListModel(self.deviceList)
        self.deviceList.setModel(self.deviceListModel)
        self.deviceList.setSelectionBehavior(QTableView.SelectRows)
        self.deviceList.selectionModel().selectionChanged.connect(self.selectedAxisChanged)
        
        self.deviceList.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)
        
        layout = QVBoxLayout()
        layout.addWidget(self.deviceList)
        layout.addWidget(clearListPushButton)
        layout.addStretch(1)
        self.DevicesGroupBox.setLayout(layout)

    def clearDeviceList(self):
       ODriveModel.clear()
       self.deviceListModel.layoutChanged.emit()
       print("Device list cleared")
    
    def makeDataTable(self):
        #self.DataGroupBox.setSizePolicy(QSizePolicy.Expanding,
        #        QSizePolicy.Expanding)
        clearListPushButton = QPushButton("Clear Dictionary")
        refreshPushButton = QPushButton("Refresh Selected")
        refreshAllPushButton = QPushButton("Refresh All")
        self.dataTable = QTableView()
        self.dataTable.setMinimumHeight(700)
        self.dataTable.setMinimumWidth(400)
        self.dataTable.setModel(ODriveDeviceDataModel())
        #self.dataTable.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)
        layout = QVBoxLayout()
        subLayout = QHBoxLayout()
        subLayout.addWidget(clearListPushButton)
        subLayout.addWidget(refreshPushButton)
        subLayout.addWidget(refreshAllPushButton)
        #subLayout.addStretch(1)
        layout.addWidget(self.dataTable)
        layout.addLayout(subLayout)
        layout.addStretch(1)
        self.dataTable.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)
        self.dataTable.show()
        self.DataGroupBox.setLayout(layout)
    
    def makeGraphArea(self):
        
        # plot some dummy data
        x = np.linspace(-10, 10, 500)
        y1, y2 = np.cos(x) * 1.00001, np.sin(x) * 1.00001
        
        self.plotWidget = QwtPlot()
        #curve1, curve2 = QwtPlotCurve("Curve 1"), QwtPlotCurve("Curve 2")
        #curve1.setData(x, y1)
        #curve2.setData(x, y2)
        #curve1.attach(self.plotWidget)
        #curve2.attach(self.plotWidget)
        self.plotWidget.resize(600, 300)
        self.plotWidget.replot()
        self.plotWidget.show()

        layout = QVBoxLayout()
        layout.addWidget(self.plotWidget)
        self.GraphGroupBox.setLayout(layout)
        
    
    def getSelectedNodeID(self):
        n = self.deviceList.currentIndex().row()
        node_id = ODriveModel.elementAt(n)
        return node_id
    
    def commandButtonPushed(self):
        cmd = self.commandComboBox.currentText()
        args = list()
        arg = self.args1TextBox.text()
        if(len(arg) > 0):
            args.append(arg)
        self.sendCommand(cmd, *args)
    
    def sendCommand(self, cmd_name, params=list()):
        try:
            foo = iter(params)
        except TypeError:
            params = [params]
        ODriveBus.send_message(self.getSelectedNodeID(), cmd_name, *params)

    def getAxisAtRow(self, row):
        return ODriveModel.elementAt(row)
        
        
    def selectedAxisChanged(self):
        self.axisSelection = self.deviceList.selectionModel().selectedRows()
        for a in self.axisSelection:
            print("Selected axis %s(%s)" % (a.row(), self.getAxisAtRow(a.row())))
            self.getAxisAtRow(a.row())
            print(self.getSelectedNodeID())
        self.dataTable.model().setNodeID(int(self.getSelectedNodeID()))
        self.dataTable.model().layoutChanged.emit()
            
    #TODO: update views for changed item
    def update(self):
        self.positionDial.setValue

class doubleDial(QwtDial):
    def __init__(self, *args):
        super(doubleDial, self).__init__(*args)
        self.value2 = 0
        self.hand2 = QwtDialSimpleNeedle(QwtDialSimpleNeedle.Arrow, False, QColor(Qt.blue))
        self.hand2.setWidth(3)
        self.setpoint = self.value()

    def drawNeedle(self, painter, center, radius, direction, colorGroup):
        super(doubleDial, self).drawNeedle(painter, center, radius, direction, colorGroup)
        direction2 = self.transform( self.value2 ) + 270.0
        self.hand2.draw(painter, center, radius*0.95, -direction2, colorGroup)
        
    # TODO: Unwrap setpoint
    def setValue(self, value1, value2):
        
        diff = value1 - self.value()
        wrap = self.setpoint % self.upperBound() 
        
        if(diff > (self.upperBound() / 2)):
            self.setpoint = value1 + self.upperBound()
        elif (diff < (self.upperBound() / -2)):
            value1 = value1 - self.upperBound()
        self.setpoint = value1 + self.upperBound()
            
        super(doubleDial, self).setValue(value)
        self.value2 = value2

#TODO: split out to separate file
class ODriveDeviceListModel(QAbstractTableModel):
    def __init__(self, parent=None):
        super(ODriveDeviceListModel, self).__init__(parent)
        self.listView = parent
        self.headers = ['Node ID','State','Error']
        self.columns = 3
        ODriveModel.setUpdateFunc(self.updateData)

        
    def headerData(self, section, orientation, role):
            if orientation == Qt.Horizontal and role == Qt.DisplayRole:
                return QVariant(self.headers[section])
            return QVariant()
            
    def index(self, row, column, parent=None):
        #node = self.nodeFromIndex(parent)
        return self.createIndex(row, column, None)

    def parent(self, parent=None):
        return QModelIndex()

    def data(self, index, role):
        if role == Qt.DecorationRole:
            return QVariant()
        if role != Qt.DisplayRole:
            return QVariant()
        node_id = ODriveModel.elementAt(index.row())
        if(node_id == -1):
            return QVariant()
        node = ODriveModel[node_id]
        try:
            if index.column() == 0:
                return QVariant(node['axis.config.node_id'])
            elif index.column() == 1:
                return QVariant(node['axis.state'])
            elif index.column() == 2:
                return QVariant(node['axis.error'])
        except KeyError as e:
            return QVariant(str(e))
        return QVariant()
            
    def columnCount(self, parent=None):
        return self.columns

    def rowCount(self, parent=None):
        return len(ODriveModel)

    def updateData(self):
        self.beginInsertRows(QModelIndex(), self.rowCount(), self.rowCount())
        self.endInsertRows()
        #self.dataChanged.emit(self.createIndex(row, 0), self.createIndex(row, 2))
        #topLeft = self.createIndex(0,0)
        #self.dataChanged.emit(topLeft, topLeft)
        self.layoutChanged.emit()
        #print("Data Updated at row %d" % row) # - now have %d rows" % (row, self.rowCount()))
        #self.endInsertRows()
        

class ODriveDeviceDataModel(QAbstractTableModel):
    def __init__(self, parent=None):
        super(ODriveDeviceDataModel, self).__init__(parent)
        self.listView = parent
        self.headers = ['data name','value','last updated']
        self.columns = 2
        self.node_id = -1
        
    def setNodeID(self, nodeID):
        self.node_id = nodeID
        ODriveModel[self.node_id].setUpdateFunc(self.updateData)
        
    def node(self):
        if self.node_id in ODriveModel:
            return ODriveModel[self.node_id] 
        else:
            return None

    def headerData(self, section, orientation, role):
            if orientation == Qt.Horizontal and role == Qt.DisplayRole:
                return QVariant(self.headers[section])
            return QVariant()
            
    def index(self, row, column, parent=None):
        #node = self.nodeFromIndex(parent)
        return self.createIndex(row, column, None)
            
    def data(self, index, role):
        if role == Qt.DecorationRole:
            return QVariant()
        if role != Qt.DisplayRole:
            return QVariant()
        
        if self.node() is None:
           return QVariant()

        key = self.node().elementAt(index.row())
        value = self.node()[key]
        
        if index.column() == 0:
            return QVariant(key)
        elif index.column() == 1:
            return QVariant(value)
        elif index.column() == 2:
            return QVariant(self.node()['axis.config.node_id'])
        else:
            return QVariant()
            
    def columnCount(self, parent=None):
        return self.columns

    def rowCount(self, parent=None):
        if(self.node() is not None):
            return len(self.node())
        else:
            return 0
        
    def updateData(self):
        self.layoutChanged.emit()


    
def process_cl_args():
    parser = argparse.ArgumentParser("ODrive-GUI-CAN.py",
        description="GUI for ODrive ODrive via CAN")
    parser.add_argument("--bustype", help="Bus type", default="socketcan", dest="bustype")
    parser.add_argument("-i", "--interface", help="CAN interface channel name", default="can0", dest="channel")

    parsed_args, unparsed_args = parser.parse_known_args()
    return parsed_args, unparsed_args

#if __name__ == '__main__':

parsed_args, unparsed_args = process_cl_args()

bus = can.interface.Bus(channel = parsed_args.channel, bustype = parsed_args.bustype)
ODriveBus =  ODriveCANBus(bus, ODriveProto, ODriveModel)

# QApplication expects the first argument to be the program name.
qt_args = sys.argv[:1] + unparsed_args
app = QApplication(qt_args)
#appctxt = ApplicationContext()
gui = ODriveGUIWindow(args=parsed_args)
gui.show()

ODriveBus.start()

UPD = UpdaterThread(ODriveBus, ODriveProto)
UPD.start()

sys.exit(app.exec_())
