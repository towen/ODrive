CAN testing tools for ODrive
============================

These tools have been tested both directly on CAN bus using a Raspberry Pi plus SKPang "PiCAN 2" hat
and also via ["cannelloni"](https://github.com/mguentner/cannelloni) to a PC.

To get list of CAN commands:

./CANsimple.py Help


GUI: Work In Progress
./ODriveGUI.py -i can0


ToDo:
====
* Rejig layout so that list of devices and key items (position, velocity, torque etc) are combined into one main table
  * Combine into a single data model. Delete config.node_id etc.
  * Allow to sort by axis or by config item ie. axisN.encoder.count_in_cpr vs encoder.count_in_cpr.axisN
* ~~background update thread~~ done - data model updates, but not yet emitting proper dataChanged signals, view only updates on mouse hover etc
* Add some nice QWT 7seg widgets for position, velocity, torque
* get graph working
